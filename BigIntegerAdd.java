import java.math.*;
import java.util.*;
class BigIntegerAdd
{
	public static void main(String...x)
	{
	    Scanner sc=new Scanner(System.in);
		System.out.print("Enter the first number : ");
		String string1=sc.nextLine();
		System.out.print("Enter the second number : ");
		String string2=sc.nextLine();
		BigInteger b1=new BigInteger(string1);
		BigInteger b2=new BigInteger(string2);
		BigInteger b3;
		
		b3=b1.add(b2);
		
		System.out.println("\n      "+b1+"\n    + "+b2+"\n-----------------------------\n      "+b3+"\n-----------------------------");
		
	}
}