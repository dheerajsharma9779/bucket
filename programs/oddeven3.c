#include<stdio.h>
int main()
{
    int number;
    printf("Enter the number ");
    scanf("%d",&number);
    if(number&1) // Bitwise operator & checks the Bitwise AND operation between number and 1 of the last bit of number is 1
                 //then it is a odd number and ANDing of that number with 1 always gives 1
    {
        printf("%d is Odd",number);
    }
    else
    {
        printf("%d is Even",number);
    }
return 0;
}
