import java.math.*;
import java.util.*;
class Big_Factorial
{
	public static void main(String a[])
	{
		
		int number,c;
		Scanner sc=new Scanner(System.in);
		System.out.print("\n");
		System.out.print("Enter the number of which you want the factorial : ");
		number=sc.nextInt();
		System.out.print("\n");
		System.out.print("Press 1 for first method and 2 for Second method : ");
		c=sc.nextInt();
		System.out.print("\n");
		
		switch(c)
		{
			case 1:
					fact1(number);
		    break;
			  
			case 2:
					System.out.println("Factorial of "+number+" : "+fact(number));
			break;
			  
			default:
			       System.out.println("Invalid Input Please press 1 or 2"); 
			break;
		}
	}
	
	static void fact1(int num)
	{
		BigInteger big=new BigInteger("1");
					
					for(int i=1;i<=num;i++)
					{
						
						big=big.multiply(BigInteger.valueOf(i));
					}
					
					String s=big.toString();
					System.out.println("Factorial of "+num+" : "+s);
	}
	
	static BigInteger fact(int num)
		{
			BigInteger big=new BigInteger("1");
			
			for(int i=1;i<=num;i++)
			{
				
				big=big.multiply(BigInteger.valueOf(i));
			}
			
			return big;
		}
}






